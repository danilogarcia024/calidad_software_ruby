#Iteradores


#En una sola linea
#Rangos <= ..
p "Iteradores con una sola Linea"
(0..3).each{|item| p item}

#En un bloque
p "Iteradores en un bloque"
(0..3).each do |item|
  p item
end

# Clase - 13/04/2016
#Otros iteradores
#Rango < ...
(20...100).each_with_index do |item, i|
  p "Indice: #{i}, Item: #{item}"
end

#Array
x = (0..3).to_a
y = (10..20).to_a
#Suma de Conjuntos
z = x + y
p "Variable z: #{z}"

#Matrices
x = [(0..5).to_a]
x.push((6..10).to_a)
x.push((11..15).to_a)
x.push("Calidad de Software")
p "Contenido de la variable x: #{x}"
p "Obtener un subconjunto de un String x[3][0..5]: #{x[3][0..5]}"

#Condificionales
x = false
y = true
if x
  p "Verdadero"
elsif y == false
  p "y es Falso"
else
  p "y es verdadero"
end

x = "a"
case x
  when "a"
   p "Es a"
  when "b"
   p "Es b"
  else
  p "Por Defecto"
end
